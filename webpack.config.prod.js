const webpack = require("webpack");
const path = require("path");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const APP = path.resolve(__dirname, "app");
const OUTPUT = path.resolve(__dirname, "target");
const devConfig = require('./webpack.config.dev.js');

const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('production')
};

const prodConfig = {
  devtool: "source-map",
  entry: [
    "babel-polyfill",
    APP + "/main.js"
  ],

  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true,
      noInfo: false,
      postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
    }),
    new ExtractTextPlugin("assets/styles.css"),
    new CopyWebpackPlugin([ { from: "../statics", allChunks: true} ]),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin(GLOBALS),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: {
          screw_ie8: true,
          keep_fnames: true
      },
      compress: {
          screw_ie8: true
      },
      comments: false,
      dead_code: true
    }),
  ],
}

module.exports = Object.assign({}, devConfig, prodConfig);
