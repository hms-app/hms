# Organizacja pracy

## Git
Każdy pracuje na swoim branchu.  


#### Mała ściąga

* Sprawdzenie stanu repo: `git status`.
* Dodanie wybranych zmian `git add -p`. *Uwaga! W ten sposób nie da się dodać nowych plików.*
* Dodania konkretnych plików do repo: `git add <sciezka do pliku>`. Ścieżkę można podejrzeć w `git status`.
* Sprawdzenie jakie pliki są dodane `git diff --staged`
* Cofnięcie git add wszystkich dodanych plików `git reset HEAD .`, lub zamiast kropki ścieżka do pliku.
* Cofnięcie lokalnie wszystkich zmian, które nie zostały dodane przez git add `git checkout -- .`
* Tworzenie commitu: `git commit -m "<treść commitu>"`
* Nazwy commitów powinny być w języku angielskim i mieć parę słów opisu czego dotyczy.
* Wysłanie commitu na serwer: `git push`.
* Zmiana brancha: `git checkout <branch>`.
* Jeśli stworzyłaś coś nowego i coś nie działa / chcesz mi dać do przetestowania i jest to coś dużego, stwórz nowych branch. Zestashuj zmiany na nowy branch: `git branch -b <nazwa>`, potem `git checkout <nazwa>`, zacommituj i wyślij przez: `git push -u origin <nazwa>`. Nazwa brancha fajnie gdyby miała angielską nazwę i nie więcej niż parę słów oddzielonych `-`
* Po wpisaniu np. `ap` i wciśnięciu `Tab` git sam dopisze resztę ścieżki.
* Sciągnięcie postępów prac z innych branchy sugeruję pobranie komendy `git up`.
* Połączenie commitów z brancha A odbywa się przez: `git merge A`
* Pobranie pojedynczego commitu z brancha A: `git cherry-pick <SHA>` (tak, nie jest wymagana nazwa brancha, jedynie SHA, czyli 7 pierwszych znaków danego commita)
* Czasem potrzebne jest "schowanie" zmian (głównie przy migrowaniu zmian między branchami): `git stash`, a ich przywrócenia `git stash pop`

* [Git cheatsheet](https://www.git-tower.com/blog/git-cheat-sheet/)

## Struktura plików

Na chwilę obecną:
```
.
├── app
│   ├── main.js
│   ├── routes.js
│   ├── server
│   │   └── server.js
│   ├── statics
│   │   ├── fonts
│   │   ├── images
│   │   └── index.ejs
│   └── views
│       ├── common
│       │   ├── common.scss
│       │   ├── error404.js
│       │   └── IndexPage.js
│       ├── homepage
│       └── Layout
│           ├── Layout.js
│           └── Layout.scss
├── docs
│   └── work-organization.md
├── package.json
├── README.md
├── target
│   ├── index.html
│   ├── index.js
│   └── styles.css
├── webpack.config.js
└── webpack-production.config.js

```


Tutaj dopiszę niedługo cały schemat.
Póki co jednak wystarczy żebyś wiedziała, że każdy widok umieszczamy w innym folderze w views. Obecnie jest tylko `Layout` a w nim plik z komponentem i .scss dotyczący zawartym w nich komponentów, o nazwie takiej samej jak folder.
Każdy kolejny folder trzeba dodać do main.js

Nazwy folderów piszemy z wielkiej litery. Wyjątkiem jest common, gdzię będą umieszczane często używane komponenty, oraz common.scss dotyczący ogólnie strony.
