import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link, browserHistory } from 'react-router';
import {} from './Dashboard.scss';


export default class Dashboard extends React.Component {
  constructor(...params) {
    super(...params);
  }

  componentWillMount() {
    this.props.role === 'admin' ?
      browserHistory.push('/shop') :
      browserHistory.push('/shop');
  }

  render() {
    // TODO: Dashboard view
    return (<div className="dashboard"></div>);
  }
}
