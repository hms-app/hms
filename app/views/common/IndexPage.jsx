import React from 'react';
import jwtDecode from 'jwt-decode';
import LandingPage from '../LandingPage/LandingPage';
import Dashboard from '../Dashboard/Dashboard';

export default class IndexPage extends React.Component {
constructor(props) {
  super(props);
}

  render() {
    const token = localStorage.getItem('id_token');

    return (
        <div className="front-page">
          {this.props.isAuthenticated ?
            <Dashboard role={jwtDecode( token ).role} /> :
            <LandingPage />
          }
        </div>
    );
  }
}
