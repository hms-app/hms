import React from 'react';
import { IndexLink, Link } from 'react-router';

const error404 = () => (
  <div className="not-found">
    <h1>404</h1>
    <h2>Strona nie znaleziona!</h2>
    <p>
      <IndexLink to="/">Powróć do strony głównej</IndexLink>
    </p>
  </div>
);

export default error404;
