import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {} from './Clients.scss';
import * as clientActions from '../../actions/clientActions';
import { T, genId } from '../../utils';


const ClientRow = (props) => {
  const info = props.info;
  return (
    <tr>
      <td>{info.email}</td>
      <td>{info.first_name}</td>
      <td>{info.last_name}</td>
      <td>{info.phone}</td>
    </tr>
  );
};


class Clients extends React.Component {
  constructor(...params) {
    super(...params);
  }

  componentDidMount() {
    this.props.actions.getClients();
  }

  clientsList = () => {
    let rows = [];
    for (const user_id in this.props.clients) {
      rows.push(<ClientRow key={genId('client')} info={this.props.clients[user_id]} />);
    }
    return rows;
  }

  render() {
    return(
       <div className="clients-list">
       <h1>{T('clients')}</h1>
        <div>
          <table className="table table-hover">
            <thead>
              <tr>
                <th>Email</th>
                <th>{T('first_name')}</th>
                <th>{T('last_name')}</th>
                <th>{T('phone')}</th>
              </tr>
            </thead>
            <tbody>
            {this.clientsList()}
            </tbody>
          </table>
        </div>
       </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    clients: state.clients
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(clientActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Clients);
