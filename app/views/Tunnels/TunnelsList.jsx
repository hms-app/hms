import React, { PropTypes } from 'react';
import TunnelTile from './TunnelTile';
import { genId, T } from '../../utils';

export default class TunnelsList extends React.Component {
  constructor(...params) {
    super(...params);
    this.state = {
      unit: 'all',
      tunnel: 'all',
    };
  }

  tunnelRow = (tunnel, index) => <TunnelTile key={index} tunnel={tunnel} tid={genId("chartContainer")} />;

  getFilterUnits = (tunnels) => tunnels.map( item => item.type );
  filterUnitOption = (item, index) => <option key={index} value={item}>{ T(item).charAt().toUpperCase() + T(item).slice(1) }</option>;
  handleUnitChange = (event) => this.setState({unit: event.target.value});

  getFilterTunnels = (tunnels) => tunnels.map( item => item.tunnel );
  filterTunnelOption = (item, index) => <option key={index} value={item}>{ item }</option>;
  handleTunnelChange = (event) => this.setState({tunnel: event.target.value});

  componentDidMount() { this.getFilterTunnels(this.props.tunnels); }
  componentDidUpdate() { this.getFilterUnits(this.props.tunnels); this.getFilterTunnels(this.props.tunnels); }

  render() {
    const filterUnits = new Set( this.getFilterUnits(this.props.tunnels) );
    const filterTunnels = new Set( this.getFilterTunnels(this.props.tunnels) );

    return (
        <div className="tunnels-preview">
          <div className="tunnels-preview_header">
            <h1>{T('measurements')}</h1>
            <div className="filters">
              <div className="filter-list">
                <p>{T('unit')}: </p>
                <select className="form-control"  onChange={this.handleUnitChange} id="unit-filter">
                  <option value="all">{T('all')}</option>
                  { [...filterUnits].map( this.filterUnitOption ) }
                </select>
              </div>
              <div className="filter-list">
                <p>{T('tunnel')}: </p>
                <select className="form-control"  onChange={this.handleTunnelChange} id="tunnel-filter">
                  <option value="all">{T('all')}</option>
                  { [...filterTunnels].map( this.filterTunnelOption ) }
                </select>
              </div>
          </div>
          </div>
        {this.props.tunnels.filter( item => (this.state.unit === item.type || this.state.unit === 'all') )
                           .filter( item => (this.state.tunnel === item.tunnel || this.state.tunnel === 'all') )
                           .map( this.tunnelRow )}
      </div>);
  }
}

TunnelsList.propTypes = {
  tunnels: PropTypes.array.isRequired
};
