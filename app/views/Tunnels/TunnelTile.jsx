import React, { PropTypes } from 'react';
import { IndexLink, Link } from 'react-router';
import Chart from 'chart.js';
import { T } from '../../utils';

export default class TunnelTile extends React.Component {
  constructor(...params) {
    super(...params);
    this.state = {
      chart: {}
    };
  }

  renderChart() {
    const ctx = document.getElementById(this.props.tid).getContext("2d");
    this.state.chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.props.tunnel.data.map(itm => itm.x),
        datasets: [{
          data: this.props.tunnel.data.map(itm => itm.y)
        }]
      },
      options: {
        responsive: false,
        legend: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero:true
          }
        }]
      }
    });
  }

  componentDidMount() { this.renderChart(); }
  componentDidUpdate() {
    this.state.chart.data.datasets[0].data = this.props.tunnel.data.map(itm => itm.y);
    this.state.chart.data.labels = this.props.tunnel.data.map(itm => itm.x);
    this.state.chart.update();
  }

  render() {
    const tunnel = this.props.tunnel;

    return (
      <div className="tunnels-preview_tile">
        <div className="tunnels-preview_tile-inner">
          <div className="tunnels-preview_tile-inner_header">
            <h2>{T('tunnel')}: {tunnel.tunnel}, {T('sensor')} nr: {tunnel.sensorID} {T('measure')}: {T(tunnel.type)}</h2>
            <Link to={`/measurements/${this.props.tunnel.id}`} className="icon-link tooltip-title" data-title={T('expand')}>
              <i className="fa fa-arrows-alt"></i>
            </Link>
          </div>
          <div className="tunnels-preview_tile-inner_chart">
            <canvas id={this.props.tid} height="190"></canvas>
          </div>
        </div>
      </div>
    );
  }
}

TunnelTile.propTypes = {
  tunnel: PropTypes.object.isRequired
};