import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as tunnelActions from '../../actions/tunnelActions';
import { T, renderChart, genId } from '../../utils';
import {} from './Tunnels.scss';


const dateFormat = 'YYYY-MM-DD HH:mm';

class TunnelSingle_ extends React.Component {
 constructor(...params) {
    super(...params);
    let from = new Date();
    this.state = {
      chart: {},
      minTimestamp: Math.max(from.setMonth(from.getMonth()-2), Math.min(...this.props.tunnel.data.map(i => i.x))*1000),
      maxTimestamp: Date.parse(new Date())
    };
  }

  renderChart() {
    const ctx = document.getElementById("chart-container").getContext("2d");
    this.state.chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.props.tunnel.data.map(itm => itm.x),
        datasets: [{
          data: this.props.tunnel.data.map(itm => itm.y)
        }]
      },
      options: {
        responsive: false,
        legend: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero:true
          }
        }]
      }
    });
  }

  componentDidMount() {
    this.renderChart();
    flatpickr(".flatpickr", {
      minDate: Math.min(...this.props.tunnel.data.map(i => i.x))*1000,
      maxDate: new Date(),
      enableTime: true,
      allowInput: true,
      time_24hr: true,
    });

    this.refs.from.value = moment(this.state.minTimestamp).format(dateFormat);
    this.refs.to.value = moment(this.state.maxTimestamp).format(dateFormat);
  }

  componentDidUpdate() {
    this.state.chart.data.datasets[0].data = this.props.tunnel.data.map(itm => itm.y);
    this.state.chart.data.labels = this.props.tunnel.data.map(itm => itm.x);
    this.state.chart.update();
  }

  handleTimeChange = (event) => {
    this.setState({
      minTimestamp: Date.parse(this.refs.from.value),
      maxTimestamp: Date.parse(this.refs.to.value)
    });
  }
  handleFilterToNow = (event) => {
    this.setState({ filteringToNow: true});
  }

  handleFilterToDate = (event) => {
    this.setState({ filteringToNow: false});
  }

  render() {
    const tunnel = this.props.tunnel;

    return(
      <div className="single-chart">
        <Link to="/measurements" data-title={T('goback')} className="icon-link tooltip-title">
          <i className="fa fa-arrow-left"></i>
        </Link>

        <h1>{T('tunnel')}: {tunnel.tunnel}, {T('measure')}: {T(tunnel.type)}</h1>
        <div className="single-chart_inner">
          <canvas id="chart-container" className="single-chart_chart" height="400"></canvas>
        </div>
        <div className="time-filter">
          <label>{T('from')}:
            <input className="flatpickr" type="datetime-local" name="from" ref="from"
                   defaultValue={moment(this.state.minTimestamp).format(dateFormat)} />
           </label>
          <label>{T('to')}:
            <input className="flatpickr" type="datetime-local" onFocus={this.handleFilterToDate} name="to" ref="to"
                   defaultValue={moment(this.state.maxTimestamp).format(dateFormat)} />
          </label>
          <label>{T('or')}</label>
          <button className="btn btn-primary" onClick={this.handleFilterToNow}>{T('now')}</button>
          <button className="btn btn-info" onClick={this.handleTimeChange}>{T('filter')}</button>
        </div>
      </div>
    );
  }
};

class TunnelSingle extends React.Component {
  constructor(...params) {
    super(...params);
  }

  componentWillMount() { this.props.actions.loadTunnels(); }

  render() {
    const tunnels = this.props.tunnels;

    return (
      <div>
       {tunnels.filter( item => (item.tunnel === this.props.params.tunnelID) )
               .filter( item => (item.sensorID === this.props.params.sensorID) )
               .map((item, index) => <TunnelSingle_ tunnel={item} key={index} />)}
      </div>
    );
  }
}

TunnelSingle.propTypes = {
  tunnels: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    tunnels: state.tunnels
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(tunnelActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TunnelSingle);
