import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tunnelActions from '../../actions/tunnelActions';
import TunnelsList from './TunnelsList';
import {} from './Tunnels.scss';

class TunnelsPage extends React.Component {
  constructor(...params) {
    super(...params);
  }


  componentWillMount() {
    this.props.actions.loadTunnels();
  }


  render() {
    return <TunnelsList tunnels={this.props.tunnels} />;
  }
}

TunnelsPage.propTypes = {
  tunnels: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    tunnels: state.tunnels
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(tunnelActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TunnelsPage);
