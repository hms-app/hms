import React from 'react';
import { IndexLink, Link } from 'react-router';
import { T } from '../../utils';
import {} from './LandingPage.scss';


export default class LandingPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
     <div className="landing-page">
        <div className="row hero">
          <h1>{T('landing-page-text')}</h1>
          <video autoPlay="true" loop="true" ref="video">
            <source src="/video/Mp4/hero.mp4" type="video/mp4" />
            <source src="/video/Webm/hero.webm" type="video/webm" />
            Your browser does not support the video tag.
          </video>
        </div>
        <div className="row welcome">
          <h1>Strona w budowie</h1>
        </div>
      </div>
    );
  }
}