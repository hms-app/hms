import React from 'react';
import { generateMenu, T } from '../../utils';


const sideBarMenu = [
  { name: T('measurements'), url: '/measurements' },
  { name: T('clients'), url: '/clients' },
  { name: T('products'), url: '/shop' }
];


export default class SideBar extends React.Component{
  render() {
    return(
      <div className="side-bar">
        <h2>Dashboard</h2>
        <ul className="side-bar_menu">
          { generateMenu(sideBarMenu) }
        </ul>
      </div>
    );
  }
}