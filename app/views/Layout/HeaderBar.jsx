import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import { logoutUser } from '../../actions/authActions';
import {} from './Layout.scss';
import { MenuIten, generateMenu, T } from '../../utils';


const adminMenu = [
  { url: '/', name: T('logout') },
  { url: 'settings', name: <i className="fa fa-cog" ></i>, dtitle: 'settings' }
];

const clientMenu = [
  { url: 'shop', name: T('shop') },
  { url: '/', name: T('logout') },
  { url: 'settings', name: <i className="fa fa-cog" ></i>, dtitle: 'settings' }
];

const guestMenu = [
  { url: '/register', name: T('register') },
  { url: '/login', name: T('login') }
];


class _Menu extends React.Component {
  constructor(...props) {
    super(...props);
  }

  render() {
    const isAuth = this.props.isAuthenticated;
    const isAdmin = this.props.isAdmin;
    return (
      <nav className="header-bar_main-navigation">
        {isAuth ?
          <ul className="header-bar_main-navigation_user">
            { 
              isAdmin ?
              generateMenu(adminMenu.map(item => item.url === '/' ? {...item, cb: this.props.actions.logoutUser, active: ' '} : item)) :
              generateMenu(clientMenu.map(item => item.url === '/' ? {...item, cb: this.props.actions.logoutUser, active: ' '} : item))
            }
          </ul> :
          <ul className="header-bar_main-navigation_user">
            {generateMenu(guestMenu)}
          </ul>
        }
      </nav>
    );
  }
}

function mapStateToProps(state) { return {}; }
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators( { logoutUser }, dispatch)
  };
}

const Menu = connect(mapStateToProps, mapDispatchToProps)(_Menu);


export default class HeaderBar extends React.Component {
  constructor(...params) {
    super(...params);
  }

  render() {
    return (
      <div className={this.props.isAuthenticated ? "header-bar" : "header-bar guest" }>
        <div id="logo" className="logo"><IndexLink to="/" activeClassName="active">HMS</IndexLink></div>
        <Menu isAuthenticated={this.props.isAuthenticated} isAdmin={this.props.isAdmin} />
      </div>
    );
  }
}
