import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import * as userActions from '../../actions/userActions';
import {} from './Layout.scss';
import { MenuIten, generateMenu, T } from '../../utils';


class FooterBar extends React.Component {
  constructor(...props) {
    super(...props);
  }

  handleLangChange = (event) => {
    this.setState({lang: event.target.value});
    this.props.actions.switchLanguage(event.target.value);
  }

  render() {
    const lang = this.props.lang;

    return (
      <div className="footer-bar">
        <span className="copy-rights">
          <a href="http://nelson-haha.api-meal.eu/">{T('footer-text')}</a>
        </span>
        <div className="change-language">
          <select className="form-control change-language_select" onChange={this.handleLangChange} value={lang}>
            <option value="pl">PL</option>
            <option value="en">EN</option>
          </select> 
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { lang: state.user.lang };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(FooterBar);
