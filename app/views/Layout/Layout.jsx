import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import * as userActions from '../../actions/userActions';
import SideBar from './SideBar';
import HeaderBar from './HeaderBar';
import FooterBar from './FooterBar';
import {} from './Layout.scss';
import { MenuIten, generateMenu, T } from '../../utils';
import jwtDecode from 'jwt-decode';


class Layout_ extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const isAuthenticated = this.props.isAuthenticated;
    const isAdmin = this.props.role === "admin";
    return (
      <div className="app-container">
        <HeaderBar isAuthenticated={isAuthenticated} isAdmin={isAdmin} />
        <div className="app-content">
          {isAdmin &&
            <SideBar />
          }
          <div className="wrapper">
            {React.cloneElement(this.props.children, { isAuthenticated })}
          </div>
        </div>
        <FooterBar />
      </div>
    );
  }
}


class Layout extends React.Component {
  constructor(...params) {
    super(...params);
  }

  render() {
    const { dispatch, isAuthenticated } = this.props;
    const token = localStorage.getItem('id_token');

    return (
      <div>
        <Layout_ isAuthenticated={isAuthenticated}
                 role={token ? jwtDecode( token ).role : 'guest'}>
          {this.props.children}
        </Layout_>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { auth } = state;
  const { isAuthenticated, id_token } = auth;
  return {
    isAuthenticated,
    id_token
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Layout);
