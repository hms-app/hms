import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import {} from './Register.scss';
import { T } from '../../utils';
import * as userActions from '../../actions/userActions';

class Register extends React.Component {
  constructor(...params) {
    super(...params);
    this.state = {
      passMatchErr: false,
      isEmail: true,
      isPassword: true
    }
  }

  handleSend = (event) => {
    if(this.refs.password.value===this.refs.repeat_password.value){
    let userData = {
        email: this.refs.email.value,
        password: this.refs.password.value,
        first_name: this.refs.first_name.value,
        last_name: this.refs.last_name.value,
        phone: this.refs.phone.value,
        address: this.refs.address.value,
        city: this.refs.city.value,
        postcode: this.refs.postcode.value,
        post: this.refs.post.value,
      }
      let filtratedData = {};
      Object.keys(userData).forEach( (key) => (userData[key] !== this.props.userData[key] && userData[key]!=="") ?
      filtratedData[key] = userData[key] :
      null);
      if(userData.email === "" || (!!this.refs.email.value.match(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)))
      {
        this.setState({isEmail: false}); 

        if(userData.password === "")
          { 
            this.setState({isPassword: false,
                           isEmail: false}); 
          }
      }
      else if(userData.password === "")
      {
        this.setState({isPassword: false});
      }
      else {
        this.setState({isPassword: true,
                      isEmail: true });
        this.props.actions.registerUser(filtratedData);
      }
    }
  }

  checkPass = (event) => {
    if(this.refs.password.value===this.refs.repeat_password.value)
      { this.setState({passMatchErr: false}); }
    else
    { this.setState({passMatchErr: true}); }
  }

  render() {
    let showPassMatchErr = this.state.passMatchErr;
    let isPass = this.state.isPassword;
    let isEmail = this.state.isEmail;
    return(
      <div className="register">
        <h1>{T('register')}</h1>
        <h3>{T('userdata')}</h3>
        <form>
          <label><span>Email:</span>
            <input type="text" name="email" ref="email" />
          </label>
          {!isEmail && <div className="alert alert-danger" role="alert">
                          <strong>{T('insert-email')}</strong>
                        </div>}
          <label><span>{T('password')}:</span>
            <input type="password" name="password" ref="password" />
          </label>
          {!isPass && <div className="alert alert-danger" role="alert">
                          <strong>{T('insert-password')}</strong>
                        </div>}
          <label><span>{T('repeat-password')}:</span>
            <input onKeyUp={this.checkPass} type="password" name="repeat_password" ref="repeat_password" />
          </label>
          {showPassMatchErr &&
            <div className="alert alert-danger" role="alert">
              <strong>{T('password-error')}</strong>
            </div>
          }
          <label><span>{T('first_name')}:</span>
            <input type="text" name="first_name" ref="first_name" />
          </label>
          <label><span>{T('last_name')}:</span>
            <input type="text" name="last_name" ref="last_name" />
          </label>
          <label><span>{T('phone')}:</span>
            <input type="text" name="phone" ref="phone"/>
          </label>
          <label><span>{T('address')}:</span>
            <input type="text" name="address" ref="address"/>
          </label>
          <label><span>{T('city')}:</span>
            <input type="text" name="city" ref="city"/>
          </label>
          <label><span>{T('postcode')}:</span>
            <input type="text" name="postcode" ref="postcode"/>
          </label>
          <label><span>{T('post')}:</span>
            <input type="text" name="post" ref="post"/>
          </label>
        </form>
        <button className="btn btn-info" onClick={this.handleSend}>{T('send')}</button>
       </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    userData: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);