import React from 'react';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions.js';
import {} from './Login.scss';
import { T } from '../../utils';

class Login extends React.Component {
  render() {
    const { onLoginClick, isFetching, errorMessage } = this.props;
    return (
      <div className="login-form">
        <h1>{T('logging')}</h1>
        <div>
          {errorMessage &&
            <div className="error-message">{T('error')}</div>
          }
          <label htmlFor="login">
            <span>Login</span>
            <input type="text" name="username" id="username" ref="username"
                   onKeyPress={this._handleKeyPress}/>
          </label>
          <label htmlFor="pass">
            <span>{T('password')}</span>
            <input type="password" name="password" id="password" ref="password"
                   onKeyPress={(event) => { if(event.key === 'Enter') this.onLogin(event)}}/>
          </label>
          <button
            className="btn btn-success login-form_login-button"
            disabled={isFetching ? true : false}
            onClick={(event) => this.onLogin(event)}>{T('send')}</button>
        </div>
      </div>
    );
  }

  onLogin(event) {
    const username = this.refs.username;
    const password = this.refs.password;
    const creds = { username: username.value.trim(), password: password.value.trim() };
    this.props.onLoginClick(creds);
  }
}

function mapStateToProps(state) {
  const { auth } = state;
  const { isAuthenticated, isFetching, errorMessage } = auth;

  return {
    isAuthenticated,
    isFetching,
    errorMessage
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onLoginClick: (creds) => dispatch(loginUser(creds))
  };
}

export default connect( mapStateToProps, mapDispatchToProps)(Login);
