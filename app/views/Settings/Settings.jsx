import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import {} from './Settings.scss';
import { T } from '../../utils';
import * as userActions from '../../actions/userActions';

class Settings extends React.Component {
  constructor(...params) {
    super(...params);
  }

  componentDidMount() {
    this.props.actions.getUserData();
  }

  handleSend = (event) => {
    const refs = this.refs;
    let userData = {
      first_name: refs.first_name.value,
      last_name: refs.last_name.value,
      phone: refs.phone.value,
      address: refs.address.value,
      city: refs.city.value,
      postcode: refs.postcode.value,
      post: refs.post.value
    }
    let changedData = {};

    Object.keys(userData).forEach( (key) => (userData[key] !== this.props.userData[key] && userData[key]!=="") ?
      changedData[key] = userData[key] :
      null);

    this.props.actions.changeUserData(changedData);
  }

  render() {
    let isUserDataLoaded = this.props.userData.email !== undefined;
    const userData = this.props.userData;

    return(
       <div className="settings">
       <h1>{T('settings')}</h1>
       {isUserDataLoaded &&
        <div>
        <h3>{T('userdata')}</h3>
        <form>
          <p>{T('first_name')}:
            <input className="form-control" type="text" defaultValue={userData.first_name} name="first_name" ref="first_name" />
          </p>
          <p>{T('last_name')}:
            <input className="form-control" type="text" defaultValue={userData.last_name} name="last_name" ref="last_name" />
          </p>
          <p>{T('phone')}:
            <input className="form-control" type="text" defaultValue={userData.phone} name="phone" ref="phone"/>
          </p>
          <p>{T('address')}:
            <input className="form-control" type="text" defaultValue={userData.address} name="address" ref="address"/>
          </p>
          <p>{T('city')}:
            <input className="form-control" type="text" defaultValue={userData.city} name="city" ref="city"/>
          </p>
          <p>{T('postcode')}:
            <input className="form-control" type="text" defaultValue={userData.postcode} name="postcode" ref="postcode"/>
          </p>
          <p>{T('post')}:
            <input className="form-control" type="text" defaultValue={userData.post} name="post" ref="post"/>
          </p>
        </form>
        <button className="btn btn-info" onClick={this.handleSend}>{T('send')}</button>
        </div>
        }
       </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    userData: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);