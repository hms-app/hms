import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {} from './Shop.scss';
import * as singleProductActions from '../../actions/singleProductAction';
import { T, genId } from '../../utils';

class BreedingRow extends React.Component {
  constructor(...params) {
    super(...params);
  }

  render(){
    return(
      <tr>
        <td>{this.props.info['tunnel_id']}</td>
        <td>{this.props.info['total']}</td>
        <td>{this.props.info['started']}</td>
        <td>{this.props.info['ended']}</td>
      </tr>
    );
  }

}

class SingleProduct extends React.Component {
  constructor(...params) {
    super(...params);
    this.state = {
      addBreeding: false
    }
  }

  breedingsList = () => {
    let rows = [];
    for (let breeding_id in this.props.product.breedings) {
      rows.push(<BreedingRow key={genId('breeding')} info={this.props.product.breedings[breeding_id]} />);
    }
    return rows;
  }

  changeToAddBreeding = () => {
    this.setState({addBreeding: true});
  }

  handleSend = (event) => {
    let breedingData = {
      product_id: this.props.params.productID,
      tunnel_id: this.refs.tunnel_id.value,
      total: this.refs.total.value,
      started: this.refs.started.value,
      ended: this.refs.ended.value,
    }
    let checkedData = {};
    Object.keys(breedingData).forEach( (key) => (breedingData[key]!=="") ?
    checkedData[key] = breedingData[key] :
    null);
    this.props.actions.registerBreeding(checkedData);
    this.setState({addBreeding: false});
  }

  componentWillMount() {
    this.props.actions.getProductInfo(this.props.params.productID);
  }
  componentWillUnmount() {
    this.props.actions.removeProduct();
  }

  render() {
    let isLoaded = this.props.product.variety != undefined;

    return(
      <div>
      {isLoaded &&
        <div className = "css_jest_glupi">
        <h1>{this.props.product.plant.charAt().toUpperCase() + this.props.product.plant.slice(1) + " " + 
             this.props.product.variety.charAt().toUpperCase() + this.props.product.variety.slice(1) } </h1>
      
      <img src={'/images/products/' + this.props.params.productID +'.jpg'} />
        <div className = "css_jest_glupi_info">
          <p><strong>{T('price')}:</strong> {this.props.product.price}</p>
            <div>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>{T('tunnel')}</th>
                  <th>{T('total')}</th>
                  <th>{T('started_date')}</th>
                  <th>{T('ended_date')}</th>
                </tr>
              </thead>
              <tbody>
              {this.breedingsList()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      }
        <div >
        { this.state.addBreeding? 
          <form>
          <p>{T('tunnel')}:
            <input className="form-control" type="text" ref="tunnel_id" />
          </p>
           <p>{T('total')}:
            <input className="form-control" type="text" ref="total" />
          </p>
          <p>{T('started_date')}:
            <input className="form-control" type="text" ref="started" />
          </p>
          <p>{T('ended_date')}:
            <input className="form-control" type="text" ref="ended" />
          </p>
          <button className="btn btn-info" onClick={this.handleSend}>{T('send')}</button>
          </form>
          :
          <button className="btn btn-info" onClick={this.changeToAddBreeding}>{T('add')}</button>
        }
        </div>
      </div>

    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    product: state.singleProduct
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(singleProductActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);

