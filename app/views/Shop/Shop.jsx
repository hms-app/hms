import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {} from './Shop.scss';
import * as productActions from '../../actions/productActions';
import { T, genId } from '../../utils';
import { Link } from 'react-router';
import jwtDecode from 'jwt-decode';


class ProductRow extends React.Component {
  constructor(...params) {
    super(...params);
  }

  render(){
    const token = localStorage.getItem('id_token');
    let isAdmin = jwtDecode( token ).role === "admin";
    return(
      <div className="product-preview_tile">
        <div className="product-preview_tile-inner">
          <div className="product-preview_tile-inner_info" >
            <label>{T('plant')}</label><p>{this.props.info['plant']}</p>
            <label>{T('variety')}</label><p>{this.props.info['variety']}</p>
            <label>{T('price')}</label><p>{this.props.info['price']}</p>
            {isAdmin &&
            <Link to={'/product/'+this.props.productID}>
              {T('more')}
            </Link>
            }
          </div>
          <div className="product-preview_tile-inner_photo"  >
            <img src={'/images/products/' + this.props.productID +'.jpg'} />
          </div>
      </div>
    </div>
    );
  }

}




class Shop extends React.Component {
  constructor(...params) {
    super(...params);
  }

  productsList = () => {
    let rows = [];
    for (let product_id in this.props.products) {
      rows.push(<ProductRow key={genId('product')} productID={product_id} info={this.props.products[product_id]} />);
    }
    return rows;
  }

  componentWillMount() {
    this.props.actions.getProducts();
  }

  render() {

    return(
      <div >
        {this.productsList()}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    products: state.products
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(productActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Shop);