/* eslint no-undef: "off" */

import React from 'react';
import { Link } from 'react-router';
import store from './../store/index';
import dictionary from './dictionary';


const lastId = {};


export function T(word) {
  return dictionary.has(word) ?
    dictionary.get(word)[store.getState().user.lang] :
    'Brak słowa w słowniku';
}


export function genId(prefix = 'id') {
  lastId[prefix] = lastId[prefix] !== undefined ? lastId[prefix] + 1 : 0;
  return `${prefix}${lastId[prefix]}`;
}


export function renderChart(data, minParam, maxParam) {
  const min = minParam || 0;
  const max = maxParam || Date.parse(new Date());

  return new CanvasJS.Chart('chart-container', {
    axisY: { includeZero: false },
    data: [{
      type: 'line',
      dataPoints: Array.from(data).map(item => ({ x: item.x * 1000, y: item.y }))
                       .filter(i => i.x > min && i.x < max),
      xValueType: 'dateTime'
    }],
    backgroundColor: 'white'
  }).render();
}


export function MenuItem(item) {
  return (
    <li>
      <Link
        activeClassName={item.active || 'active'}
        to={item.url}
        onClick={item.cb}
        className={item.dtitle && 'tooltip-title'}
        data-title={item.dtitle}
      >
        {item.children}
      </Link>
    </li>
  );
}


export function generateMenu(data) {
  return data.map(item => (
    <MenuItem
      url={item.url}
      active={item.active}
      cb={item.cb}
      dtitle={item.dtitle}
      key={genId('menuItem')}
    >
      {item.name}
    </MenuItem>
  ));
}
