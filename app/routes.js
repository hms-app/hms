import React from 'react';
import { Route, IndexRoute } from 'react-router';
import jwtDecode from 'jwt-decode';
import Layout from './views/Layout/Layout';
import Login from './views/Auth/Login';
import Register from './views/Auth/Register';
import IndexPage from './views/common/IndexPage';
import TunnelsPage from './views/Tunnels/TunnelsPage';
import TunnelSingle from './views/Tunnels/TunnelSingle';
import error404 from './views/common/error404';
import Settings from './views/Settings/Settings';
import Clients from './views/Clients/Clients';
import Shop from './views/Shop/Shop';
import SingleProduct from './views/Shop/SingleProduct';

/* eslint max-len: "off", no-use-before-define: "off" */

const routes = (
  <Route path="/" component={Layout}>
    <IndexRoute component={IndexPage} />
    <Route path="measurements" component={TunnelsPage} onEnter={requireAdmin} />
    <Route path="measurements/:userID/:tunnelID/:sensorID" component={TunnelSingle} onEnter={requireAdmin} />
    <Route path="clients" component={Clients} onEnter={requireAdmin} />
    <Route path="shop" component={Shop} onEnter={requireAuth} />
    <Route path="product/:productID" component={SingleProduct} onEnter={requireAdmin} />
    <Route path="login" component={Login} onEnter={requireGuest} />
    <Route path="register" component={Register} onEnter={requireGuest} />
    <Route path="settings" component={Settings} onEnter={requireAuth} />
    <Route path="*" component={error404} />
  </Route>
);

function requireGuest(nextState, replaceState) {
  if (localStorage.getItem('id_token'))
    replaceState({ nextPathname: nextState.location.pathname }, '/');
}

function requireAuth(nextState, replaceState) {
  if (!localStorage.getItem('id_token'))
    replaceState({ nextPathname: nextState.location.pathname }, '/login');
}

function requireAdmin(nextState, replaceState) {
  const token = localStorage.getItem('id_token');
  if (!token || jwtDecode(token).role !== 'admin')
    replaceState({ nextPathname: nextState.location.pathname }, '/login');
}


export default routes;
