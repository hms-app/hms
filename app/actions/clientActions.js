import axios from 'axios';
import API_ADDRESS from '../utils/apiaddress';
import * as t from '../actions/actionTypes';


function loadClientsSuccess(clientsData) {
  return {
    type: t.CLIENTS_LOAD_SUCCESS,
    clientsData
  };
}


export function getClients() {
  return dispatch =>
    axios.get(`${API_ADDRESS}/.api/users`)
      .then((res) => {
        dispatch(loadClientsSuccess(res.data));
      })
      .catch((err) => { throw err; });
}
