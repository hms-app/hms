import axios from 'axios';
import { browserHistory } from 'react-router';
import * as t from './actionTypes';
import API_ADRESS from '../utils/apiaddress';


function loadProductInfoSuccess(productInfo) {
  return {
    type: t.LOAD_PRODUCT_SUCCESS,
    productInfo
  };
}

function removeProductSuccess() {
  return {
    type: t.REMOVE_PRODUCT_SUCCESS
  };
}

function registerBreedingSuccess(breedingData) {
  return {
    type: t.REGISTER_BREEDING_SUCCESS,
    breedingData
  };
}


export function getProductInfo(productId) {
  return dispatch =>
    axios.get(`${API_ADRESS}/.api/products/${productId}`)
      .then(res => dispatch(loadProductInfoSuccess(res.data)))
      .catch((err) => { throw err; });
}


export function registerBreeding(breedingData) {
  return dispatch =>
    axios.post(`${API_ADRESS}/.api/breedings`, breedingData)
      .then(browserHistory.push('/shop'))
      .then(() => dispatch(registerBreedingSuccess(breedingData)))
      .catch((err) => { throw err; });
}

export function removeProduct() {
  return dispatch =>
    dispatch(removeProductSuccess());
}
