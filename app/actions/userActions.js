import axios from 'axios';
import jwtDecode from 'jwt-decode';
import * as t from '../actions/actionTypes';
import { loginUser } from '../actions/authActions';
import API_ADRESS from '../utils/apiaddress';


function changeUserDataSuccess(changedUserData) {
  return {
    type: t.CHANGE_USER_DATA_SUCCESS,
    changedUserData
  };
}


function loadUserDataSuccess(userData) {
  return {
    type: t.LOAD_USER_DATA_SUCCESS,
    userData
  };
}

function registerUserDataSuccess(newUserData) {
  return {
    type: t.REGISTER_USER_DATA_SUCCESS,
    newUserData
  };
}


export function getUserData() {
  const token = jwtDecode(localStorage.getItem('id_token'));
  return dispatch =>
  axios.get(`${API_ADRESS}/.api/users/${token.user_id}`)
    .then(res => dispatch(loadUserDataSuccess(res.data)))
    .catch((err) => { throw err; });
}


export function changeUserData(changedUserData) {
  const token = jwtDecode(localStorage.getItem('id_token'));
  return dispatch =>
  axios.put(`${API_ADRESS}/.api/users/${token.user_id}`, changedUserData)
    .then(res => dispatch(changeUserDataSuccess(res.data)))
    .catch((err) => { throw err; });
}


export function registerUser(newUserData) {
  return (dispatch) => {
    axios.post(`${API_ADRESS}/.api/users`, newUserData)
      .then((res) => {
        localStorage.setItem('id_token', res.data.id_token);
        const loginData = {
          username: newUserData.email,
          password: newUserData.password
        };

        dispatch(loginUser(loginData));
        return res.data.id_token;
      })
      .then(newData => dispatch(registerUserDataSuccess(newData)))
      .catch((err) => { throw err; });
  };
}


export function switchLanguage(language) {
  return {
    type: t.SWITCH_LANGUAGE,
    language
  };
}
