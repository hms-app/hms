import axios from 'axios';
import * as t from './actionTypes';
import API_ADRESS from '../utils/apiaddress';


function loadProductsSuccess(products) {
  return {
    type: t.LOAD_PRODUCTS_SUCCESS,
    products
  };
}


export function getProducts() {
  return dispatch =>
  axios.get(`${API_ADRESS}/.api/products`)
    .then(res => dispatch(loadProductsSuccess(res.data)))
    .catch((err) => { throw err; });
}
