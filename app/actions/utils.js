import RethinkdbWebsocketClient from 'rethinkdb-websocket-client';


const r = RethinkdbWebsocketClient.rethinkdb;
const options = {
  host: "localhost",
  port: 7559,
  path: '/db',
  secure: false,
  db: 'hms',
  simulatedLatencyMs: 100
};
const connection = RethinkdbWebsocketClient.connect(options);


export function loadAndSubscribe(table, dispatch, loadDispatch, updateDispatch) {
  connection.then( (conn) => {
    r.table(table).run(conn, (err, cursor) =>
      cursor.toArray().then( (results) =>
        dispatch(loadDispatch(results))
      ));

    r.table(table).changes().run(conn, (err, cursor) =>
      cursor.each( (err, results) =>
        dispatch(updateDispatch(results.new_val))
      ));
  });
}