import axios from 'axios';
import * as t from './actionTypes';
import API_ADRESS from '../utils/apiaddress';
import { loadAndSubscribe } from './utils.js';


function loadTunnelsSuccess(tunnels) {
  return { type: t.LOAD_TUNNELS_SUCCESS, tunnels };
}

function updateTunnelsSuccess(tunnels) {
  return { type: t.UPDATE_TUNNELS_SUCCESS, tunnels };
}


export function loadTunnels() {
  return dispatch =>
    loadAndSubscribe('measurements',
                     dispatch,
                     loadTunnelsSuccess,
                     updateTunnelsSuccess);
}