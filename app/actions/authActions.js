import axios from 'axios';
import { browserHistory } from 'react-router';
import * as t from './actionTypes';
import { T } from '../utils';
import API_ADRESS from '../utils/apiaddress';


function requestLogin(creds) {
  return {
    type: t.LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds
  };
}

function receiveLogin(user) {
  return {
    type: t.LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    id_token: user.id_token
  };
}

function loginError(message) {
  return {
    type: t.LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message: T(message)
  };
}

function requestLogout() {
  return {
    type: t.LOGOUT_REQUEST,
    isFetching: true,
    isAuthenticated: true
  };
}

function receiveLogout() {
  return {
    type: t.LOGOUT_SUCCESS,
    isFetching: false,
    isAuthenticated: false
  };
}


export function loginUser(creds) {
  if (creds.username === '' || creds.password === '')
    return dispatch => dispatch(loginError('missing-data'));

  const data = { username: creds.username, password: creds.password };
  return (dispatch) => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds));

    return axios.post(
      `${API_ADRESS}/.api/sessions/create`, data)
      .then((res) => {
        localStorage.setItem('id_token', res.data.id_token);
        dispatch(receiveLogin(res.data));
        browserHistory.push('/');
      })
      .catch((err) => {
        if (err.response.status === 400 || err.response.status === 401)
          dispatch(loginError(err.response.data));
      });
  };
}


export function logoutUser() {
  return (dispatch) => {
    dispatch(requestLogout());
    localStorage.removeItem('id_token');
    dispatch(receiveLogout());
  };
}
