import * as t from '../actions/actionTypes';

export default function singleProduct(state = {}, action) {
  switch (action.type) {
    case t.LOAD_PRODUCT_SUCCESS:
      return action.productInfo;
    case t.REMOVE_PRODUCT_DATA:
      return {};
    case t.REGISTER_BREEDING_SUCCESS:
      return state;
    default:
      return state;
  }
}
