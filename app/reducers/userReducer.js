import * as t from '../actions/actionTypes';

const defaultUserState = {
  lang: localStorage.getItem('lang') || 'pl'
};

export default function user(state = defaultUserState, action) {
  switch (action.type) {
    case t.CHANGE_USER_DATA_SUCCESS:
      return { ...state, ...action.changedUserData };
    case t.LOAD_USER_DATA_SUCCESS:
      return { ...state, ...action.userData };
    case t.REGISTER_USER_DATA_SUCCESS:
      return { ...state };
    case t.LOGOUT_SUCCESS:
      return {};
    case t.SWITCH_LANGUAGE:
      localStorage.setItem('lang', action.language);
      location.reload();
      return action.language;
    default:
      return state;
  }
}
