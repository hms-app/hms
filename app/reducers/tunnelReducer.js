import * as t from '../actions/actionTypes';

export default function tunnelsReducer(state = [], action) {
  switch (action.type) {
    case t.LOAD_TUNNELS_SUCCESS:
      return action.tunnels;
    case t.UPDATE_TUNNELS_SUCCESS:
      let objIndex = state.findIndex(itm => itm.id === action.tunnels.id);
      return objIndex !== -1 ?
        state.map( (itm, idx) => idx === objIndex ? {...itm, ...action.tunnels} : itm) :
        [...state, ...[action.tunnels]];
    default:
      return state;
  }
}
