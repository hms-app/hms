import { combineReducers } from 'redux';
import tunnels from './tunnelReducer';
import auth from './authReducer';
import user from './userReducer';
import clients from './clientReducer';
import products from './productReducer';
import singleProduct from './singleProductReducer';

const rootReducer = combineReducers({
  auth,
  tunnels,
  user,
  clients,
  products,
  singleProduct
});

export default rootReducer;
