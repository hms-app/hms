import * as t from '../actions/actionTypes';

export default function products(state = {}, action) {
  switch (action.type) {
    case t.LOAD_PRODUCTS_SUCCESS:
      return action.products;
    default:
      return state;
  }
}
