import * as t from '../actions/actionTypes';

export default function clients(state = {}, action) {
  switch (action.type) {
    case t.CLIENTS_LOAD_SUCCESS:
      return { ...state, ...action.clientsData };
    default:
      return state;
  }
}
