/* eslint no-console: 0 */

import path from 'path';
import { Server } from 'http';
import Express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import routes from '../routes';
import error404 from '../views/common/error404';

const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./../../webpack.config.js');

// initialize server
const app = new Express();
const server = new Server(app);

const port = process.env.PORT || 8000;
const env = process.env.NODE_ENV || 'development';

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../../statics'));


if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(config);

  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
  }));
  app.use(webpackHotMiddleware(compiler));
} else {
  app.use(Express.static(path.join('..', '..', __dirname, 'static')));
}

// universal routing and rendering
app.get('*', (req, res) => {
  match(
    { routes, location: req.url },
    (err, redirectLocation, renderProps) => {
      //  in case of error, display the error message
      if (err) {
        return res.status(500).send(err.message);
      }

      // in case of redirect, propagate the redirect to the browser
      if (redirectLocation) {
        return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
      }

      // generate the React markup for the current route
      let markup;
      if (renderProps) {
        // if the current route mathched we have renderProps
        markup = renderToString(<RouterContext {...renderProps} />);
      } else {
        // otherwise we render 404 page
        markup = renderToString(<error404 />);
        res.status(404);
      }

      // render the index template with the embedded React markup
      return res.render('index', { markup });
    },
  );
});

// start the server
server.listen(port, (err) => {
  console.info(`Server running on localhost:${port} [${env}]`);

  if (err) {
    console.error(err);
  }
});
