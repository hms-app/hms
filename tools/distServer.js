import express from 'express';
import path from 'path';
import open from 'open';

const port = 8080;
const app = express();

app.use(express.static('target'));

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log(`App working on: http://localhost:${port}`);
  }
});
