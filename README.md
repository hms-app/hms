# HMS - Hodowla Management System

## README w przygotowaniu

### Wymagania
Wymagany jest jedynie Node oraz NPM

### Instalacja
`npm install`

### Uruchomienie projektu
`npm start`  
Projekt będzie dostępny pod adresem: `localhost:8080`

### Linting
Aby nie zepsuć lintowania koniecznie trzeba ustawić znak zakończenia linii na LF (unix), zamiast CRLF (windows).
Sublime text: `View > Line Endings > Unix`   
  
Przed pushem poprawek dobrze zrobić lintowanie (sprawdzenie poprawnego stylu kodu): `npm run lint`

### Organizacja pracy
`docs/work-organization.md`

### Plik z założeniami projektu
[HMS.docx](https://docs.google.com/document/d/1voqjnduazP65cIgwludx8J45MTUb3ybnsimh5QhuWu0)