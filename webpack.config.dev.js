const webpack = require("webpack");
const path = require("path");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const APP = path.resolve(__dirname, "app");
const OUTPUT = path.resolve(__dirname, "target");

const styleLoaders = [
  {
    loader: 'css-loader'
  },
  {
    loader: 'postcss-loader',
    options: {
      plugins: () => [ autoprefixer({ browsers: ['last 2 versions'] }) ]
    }
  },
  {
    loader: 'sass-loader'
  },
];

const config = {
  devtool: "cheap-module-eval-source-map",
  context: APP,

  entry: [
    "babel-polyfill",
    "webpack-dev-server/client?http://localhost:8080",
    "webpack/hot/only-dev-server",
    APP + "/main.js"
  ],

  output: {
    path: OUTPUT,
    filename: "index.js",
    publicPath: "/"
  },

  devServer: {
    contentBase: "./app",
    inline: true,
    port: 8080,
    // It suppress error shown in console, so it has to be set to false.
    quiet: false,
    // It suppress everything except error, so it has to be set to false as well
    // to see success build.
    noInfo: false,
    stats: {
      // Config for minimal console.log mess.
      assets: false,
      colors: true,
      version: false,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false
    }
  },

  target: "web",
  resolve: {
    extensions: [".js", ".jsx"]
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true,
      noInfo: false,
      postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
    }),
    new ExtractTextPlugin("assets/styles.css"),
    new CopyWebpackPlugin([ { from: "../statics", allChunks: true} ]),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    })
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: { presets: ['es2015', 'stage-0', 'react'] }
      },
      { test: /\.html$/, exclude: /node_modules/, loader: "html" },
      { test: /\.css$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{ loader: 'css-loader' }]
      }) },
      { test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: styleLoaders
      }) },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
      { test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml' },
    ]
  },

}

module.exports = config;
